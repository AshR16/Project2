let each = require("../each.js")

const items = [1, 2, 3, 4, 5, 5]

each(items,(ele,i)=>{
    console.log(`Element ${ele} index ${i}`)
})