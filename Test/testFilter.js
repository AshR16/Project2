let filter = require("../filter.js");

const items = [1, 2, 3, 4, 5, 5];

let result = filter(items, (ele) => {
  return ele > 4;
});

console.log(result);
