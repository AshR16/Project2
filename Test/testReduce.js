let reduce = require("../reduce.js")

const items = [1, 2, 3, 4, 5, 5]

reduce(items,(startingValue,element)=>{
    console.log(`starting value is ${startingValue} and the element passed is ${element}`)
},2)

// when we don't pass the starting value

reduce(items,(startingValue,element)=>{
    console.log(`starting value is ${startingValue} and the element passed is ${element}`)
})