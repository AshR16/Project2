function reduce(elements, cb, startingValue){
    
    if(startingValue===undefined){
        startingValue=elements[0]
    }
    for(let i=0;i<elements.length;i++){
        startingValue+=elements[i]
        cb(startingValue,elements[i])
    }
}

module.exports = reduce;