function find(elements, cb) {
  let result;
  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i])) {
      result = elements[i];
      break;
    }
  }

  return result;
}

module.exports = find;
